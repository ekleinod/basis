# Der Basis-Stil

Der Basis-Stil ist ein LaTeX-Stil mit Definitionen und Templates für LaTeX-Dokumente, -Präsentationen und -Briefe.

Änderungen: [changelog.md](changelog.md)

## Git-Workflow

"stable mainline" nach <http://www.bitsnbites.eu/a-stable-mainline-branching-model-for-git/>

## Lizenz

Creative-Commons-Lizenz Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 4.0 International

- [LICENSE](LICENSE)
- <http://creativecommons.org/licenses/by-nc-sa/4.0/deed.de>
