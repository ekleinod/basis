Quotes Language:		german
Base Header Level:	3
latex leader:				basis-mmd-scrlttr2
MyOptions:					font=roboto
Title:							Betreff
Date:
Author:							Autorname
MyBriefkopf:
MyStrasse:					Straße
MyPLZ:							PLZ
MyOrt:							Ort
MyTelefon:
MyHandy:
MyEmail:
MyHomepage:
MyAdresszusatz:
MyLogo:
MySponsorLogo:
MyAnA:							Empfänger
MyAnB:							Empfängerstraße
MyAnC:							Empfängerort
MyAnD:
MyAnE:
MyAnrede:
MyIhrZeichen:
MyIhrSchreiben:
MyUnserZeichen:
MyRechnung:
MyGruss:
MyUnterschrift:
MyPS:
MyAnlage:
MyCC:								
latex header:				\input{basis-mmd-style}
latex begin:				basis-mmd-begin-lttr
latex footer:				basis-mmd-end-lttr

Der Brieftext
