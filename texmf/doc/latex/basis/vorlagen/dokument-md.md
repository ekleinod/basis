Quotes Language:		german
Base Header Level:	3
latex leader:				basis-mmd-scrartcl
MyOptions:
Title:							Dokumenttitel
Date:
Author:							Autorname
MySubtitle:
MyVersion:
MyStrasse:
MyPLZ:
MyOrt:
MyTelefon:
MyHandy:
MyEmail:
MyHomepage:
MyAdresszusatz:
MyTitelzusatz:
MyLogo:
MySponsorlogo:			
latex header:				\input{basis-mmd-style}
latex begin:				basis-mmd-begin-doc

`\maketitle`{=latex}

{{TOC}}
`\cleardoublepage`{=latex}

# Kapitel 1 [sec:kap1]

Hier sollte richtiger Inhalt stehen.
