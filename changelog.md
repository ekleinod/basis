# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).


## [Unreleased]

### Changed

- using `fontawesome5` instead of `fontawesome`

### Fixed

- using `\isundef` of *etoolbox* instead of `\Ifundefinedorrelax`


## [2.0.0] - 2020-05-24

Complete refactoring of the code and options.

### Added

- font-Optionen "nimbus" und "source"
- Beispieldateien für Schriften
- bookmark-Paket
- changelog
- dhua-Abkürzungen: bzw., ca., etc., evtl., mind. und usw.
- siunitx-Voreinstellungen
- ant-Unterstützung aus edgeutils

### Changed

- Schriftarten umgestaltet
- Schriften funktionieren jetzt auch mit XeLaTeX und LuaLaTeX
- scrpage2 -> scrlayer-scrpage
- Ant-Skript
- ifxetex, ifluatex -> iftex (Pakete)
- Titelseitendefinition erst bei Beginn des Dokuments
- Definitionen für Briefe und Basis stärker getrennt
- Vorlagen aktualisiert

### Removed

- font-Optionen "mathpazo", "hfold", "original" und "times"
- Option "hyperdriver"
- mmd für changes-Paket
- tabu-Paket (wird nicht mehr entwickelt)

### Fixed

- Multimarkdown-Beispiele an MMD 6 angepasst
- babel-Fehler \renewcommand -> \def
- veraltete KomaScript Aufrufe \ifkomavarempty -> \Ifkomavarempty


## [1.0.0] - 2019-01-13

### Added

- paper-Option
- tabulary-Option
- noemail-Option
- siunitx-Paket


## [0.14] - 2016-09-06

### Added

- Briefe große erste Seite
- Beamer-Theme
- orientation-Option
- nosponsorlogo-Option
- description-Listen-Formatierung

### Changed

- xcolor mit Option table


## [0.13] - 2014-11-13

### Added

- eigener Befehl \basBox für Boxen: leer, checked und crossed

### Changed

- xcolor mit Option svgnames


## [0.12] - 2014-06-01

### Added

- Regelsatz-Option rules
- Kein-Author-Option noauthor
- Logo-Positionierung bei notitlepage verbessert
- Sponsorlogo

### Changed

- changes-Paket in mmd


## [0.11] - 2014-01-11

### Added

- Schriftgröße-Option fontsize


## [0.10] - 2014-01-10

### Added

- Font-Option droid

### Fixed

- Seitennummer


## [0.9] - 2014-01-04

### Changed

- Logo im Brief (Infospaltenlayouts)


## [0.8] - 2014-01-04

### Changed

- Separater Text für Kopfzeile im Infospaltenlayout


## [0.7] - 2014-01-04

### Added

- Einfache Gliederung für Briefe


## [0.6] - 2013-12-29

### Added

- Multimarkdown-Unterstützung


## [0.5] - 2013-12-17

### Fixed

- Fußzeile auf geraden Seiten korrekt formatiert


## [0.4] - 2013-12-16

### Changed

- Redesign des Programmcodes
- verbesserte Templates

### Removed

- Entfernen überflüssiger/obsoleter Optionen


## [0.3] - 2013-11-26

### Added

- utf8 als Encoding gesetzt

### Fixed

- PDF-Titel korrigiert (Untertitel wurde nicht korrekt gesetzt)


## [0.2] - 2007-01-16

### Added

- Flattersatz in Briefen
- Befehl \textsubscript eingefügt
- Optionen nojura, nohyper, hypercolor, hyperdriver, fixme
- Optionen font zur Fontumschaltung
- Optionen bewerbung zur Layoutumschaltung
- Option onehalfspacing eingeführt und Seitenlayout nach setspace-Umschaltung neu berechnet
- Paket fontenc mit T1 für T1-Schriften (Umlautbehandlung)
- Paket microtype eingebunden
- Schrift "Luxi Mono" als tt-Schrift

### Changed

- Definitionen an ifthen-Paket angepasst
- Überschriften von longtable-Tabellen angepaßt
- Umstellung auf xkeyval
- Option entwurf in draft umbenannt
- Option ibidem für jurabib ausgeweitet
- jurabib-Optionen in Konfigurationsdatei ausgelagert, dafür Vorlage erstellt


## [0.1] - 2006-05-14

Initiale Version

### Added

- Einbindung der wichtigsten Pakete
- Schriftarten PostScript, bis auf Marvo-Schrift für Euro-Symbol
- Vorlagen für Artikel, Bücher und Briefe
- eigene Indexvorlage
