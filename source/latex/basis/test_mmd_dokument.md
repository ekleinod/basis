Quotes Language:		german
Base Header Level:	3
latex leader:				basis-mmd-scrartcl
MyOptions:					font=roboto, tabulary
Title:							Dokumenttitel
Date:								Datum
Author:							Autorname
MySubtitle:					Untertitel
MyVersion:					Version 1.0
MyStrasse:					Straße
MyPLZ:							PLZ
MyOrt:							Ort
MyTelefon:					+49 30 1223344
MyHandy:						+49 177 1223344
MyEmail:						abc@abc.de
MyHomepage:					www.abc.de
MyAdresszusatz:			Adresszusatz
MyTitelzusatz:			Titelzusatz
MyLogo:							testlogo
MySponsorlogo:			joola-green
latex header:				\input{basis-mmd-style}
latex begin:				basis-mmd-begin-doc

`\maketitle`{=latex}

{{TOC}}
`\cleardoublepage`{=latex}

# Überschrift

## Unterüberschrift [sec:unter:unter]

Mit etwas Text.
Und noch mehr Text.

- Listenpunkt
- Listenpunkt
	- Listenpunkt
	- Listenpunkt
- Listenpunkt
- Listenpunkt

# Aufzählungen

1. eins
2. zwei
4. drei
4. vier

siehe [][sec:unter:unter]

# Anführungszeichen

"Jawohl", sagte er.

# Tabellen

| | Text | Bedeutung |
|--|:--:|--:|
| X | ein X | Buchstabe |
| 1 | eine 1 | Das ist eine Zahl |
[Tabellenunterschrift][label2]

| | Text | Bedeutung |
|--|:--:|--:|
| X | ein X | Buchstabe |
| 1 | eine 1 | Das ist eine Zahl Das ist eine Zahl Das ist eine Zahl Das ist eine Zahl Das ist eine Zahl Das ist eine Zahl |
[Tabellenunterschrift][label2]
